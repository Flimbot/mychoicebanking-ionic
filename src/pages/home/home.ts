import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyApp } from '../../app/app.component'


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  credentials = {accountid:'', password:''};
  public loadingController: LoadingController;
  public alertController: AlertController;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.loadingController = loadingCtrl;
    this.alertController = alertCtrl;
  }

  login() {
    var accountid = this.credentials.accountid;
    var password = this.credentials.password;

    let loader = this.loadingController.create({content: "Logging in"});
    loader.present();

    var loginProcess = new Promise(function(resolve, reject) {
      if(AuthServiceProvider.login(accountid,password)){
        resolve('Logged in successfully');        
      }
      else {
        reject('Failed to log in');
      }
    });

    var alerter = this.alertController.create({'title': 'Sorry', 'subTitle': null, buttons: ['Ok']});
    
    loginProcess.then(function(message){
      loader.dismiss();
      this.navCtrl.push(MenuPage);
    }, function(message) {
      loader.dismiss();
      alerter.setSubTitle(message);
      alerter.present();
    });
  }
}

