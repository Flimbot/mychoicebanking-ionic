import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduledTransactionsPage } from './scheduled-transactions';

@NgModule({
  declarations: [
    ScheduledTransactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ScheduledTransactionsPage),
  ],
})
export class ScheduledTransactionsPageModule {}
