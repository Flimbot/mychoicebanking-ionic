import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

// ML : Maybe use this for inspiration https://devdactic.com/login-ionic-2/

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {

  static sessionDetails = {};

  constructor(public http: Http) {
    //console.log('Hello AuthServiceProvider Provider');
  }

  static login(accountid, password) {
    return false;
  }

  static logout() {
    return false;
  }
}